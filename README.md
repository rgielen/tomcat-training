# Tomcat Training Supporting Project

This project contains setup used in my Tomcat training courses.
It works both as a starter for seeding an actual course, as well as a log of historic courses.

If you attended a training, please be aware that you will have to checkout the branch matching your training course.
The branches will follow _city-date_ naming where city is matches course location and date the (rough) date when the course took place.

Actual branches:

 - master - the seed project
 - [graz-201806](https://bitbucket.org/rgielen/tomcat-training/branch/graz-201806) - support for training in Graz June 2018
 - [graz-201705](https://bitbucket.org/rgielen/tomcat-training/branch/graz-201705) - support for training in Graz May 2017
 - [wien-201610](https://bitbucket.org/rgielen/tomcat-training/branch/wien-201610) - support for training in Wien October 2016
 - [wien-201609](https://bitbucket.org/rgielen/tomcat-training/branch/wien-201609) - support for training in Wien September 2016
 - [graz-201605](https://bitbucket.org/rgielen/tomcat-training/branch/graz-201605) - support for training in Graz May 2016
 
 The specific training contents [may be downloaded even without the usage of Git as a tool](https://bitbucket.org/rgielen/tomcat-training/downloads?tab=branches).
 
## Useful Links

Linux service setups

  - [systemd](https://www.digitalocean.com/community/tutorials/how-to-install-apache-tomcat-8-on-centos-7)
  - [SysV init](https://www.mulesoft.com/tcat/tomcat-on-linux-installation-and-configuration)

PSI Probe Manager App
  - [Installation HowTo](https://github.com/psi-probe/psi-probe/wiki/InstallationApacheTomcat)
  
